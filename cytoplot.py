 #! /bin/sh
""":"
exec ipython -i $0 ${1+"$@"}
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import json
import os
import seaborn as sns

palettelist = sns.color_palette("bright",10)
red = palettelist[2]
blue = palettelist[0]
green = palettelist[1] 


mpl.rcParams["pdf.fonttype"] = 42


dp = []  # data points contains all the points loaded


def importdata(rootfolder=".", subfolder="all"):
    global dp
    Nfolders = 0
    Nconds = 0
    Npoints = 0
    for file in os.listdir(rootfolder):
        print file
        if os.path.isdir(file):
            if (subfolder == "all") or (subfolder == file):
                Nfolders = Nfolders + 1
                if os.path.isfile(file+"/"+file+".json"):
                    with open(file + "/" + file + ".json") as f:
                        print "Reading file: " + file + "/" + file + ".json"
                        json_data = json.load(f)
                        for el in json_data:
                            Nconds = Nconds + 1
                            print el
                            if len(json_data[el]["max"]) == 1:
                                Npoints = Npoints + 1
                            elif len(json_data[el]["max"]) == 2:
                                Npoints = Npoints + 3
                                # this counts the minimum
                            json_data[el]["folder"] = file
                            json_data[el]["condition"] = el
                            dp.append(json_data[el])
    print "Loaded " + str(Npoints) + " points from ",
    print str(Nconds) + " conditions ",
    print "in " + str(Nfolders) + " files"


def condequal(conditionlist):  # get index of dp that follow certain equalities

    indexes = []
    for ix, x in enumerate(dp):
        if all(x[condition[0]] == condition[1] for condition in conditionlist):
            indexes.append(ix)
    return indexes

def condgreat(conditionlist):  # get index of dp that follow certain inequality

    indexes = []
    for ix, x in enumerate(dp):
        if all(x[condition[0]] >= condition[1] for condition in conditionlist):
            indexes.append(ix)
    return indexes

def condless(conditionlist):  # get index of dp that follow certain inequalities

    indexes = []
    for ix, x in enumerate(dp):
        if all(x[condition[0]] <= condition[1] for condition in conditionlist):
            indexes.append(ix)
    return indexes

def plotidx(idxlist,xaxis,yaxis):

    xaxisset=set()
    for idx in idxlist:
        xaxvalue = dp[idx][xaxis]
        if xaxvalue != -1:  # make !=0 for non-zero values do not have problems in logscale
            xaxisset.add(xaxvalue) # useful to know smallest value
            maxvalues=[]
            for X in dp[idx]["max"]:
                maxvalues.append(X["xpos"])
            plt.plot(xaxvalue, np.mean(maxvalues), 'o', color=red,alpha = 1.0)
            print dp[idx]["folder"], dp[idx][xaxis], X["xpos"]

    #        if "min" in dp[idx].keys():
    #            for X in dp[idx]["min"]:
    #                plt.plot(xaxvalue, X["xpos"], 'o', color=blue, alpha = 0.5)

    # for idx in idxlist:
    #     xaxvalue = dp[idx][xaxis]
    #     if (xaxvalue==0):  # zero is plotted specially
    #         xaxvalue = min(xaxisset)/2.0
    #         for X in dp[idx]["max"]:
    #             plt.plot(xaxvalue, X["xpos"], '<', color=red,alpha = 0.5)
    #             print dp[idx]["folder"], dp[idx][xaxis], X["xpos"]

    #         if "min" in dp[idx].keys():
    #             for X in dp[idx]["min"]:
    #                 plt.plot(xaxvalue, X["xpos"], '<', color=blue, alpha = 0.5)

#    plt.gca().set_xscale("log")
    plt.gca().set_yscale("log")
    plt.gca().set_xlim(-10,1010)

#    plt.gca().set_xlim([min(xaxisset)/4.0,max(xaxisset)*2.0])
    plt.savefig("inductiontime.pdf")
    plt.show()

