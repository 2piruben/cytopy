# Apply gates for cytopy
import matplotlib.path as mplPath
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


palettelist = sns.color_palette("bright",10)
palette = iter(palettelist)

def Add_Gate(P, channelx, channely, gatelist):
# P is list of points defining a polygon that will restrict points in channel  

    gatelist.append({"polygon": P, "channelx": channelx, "channely": channely, "color": next(palette)})


def GatedIndexes(X, P, channelx, channely):     
    polygon = mplPath.Path(P)
    points = zip(X[channelx], X[channely])
    return polygon.contains_points(points)


def GatedIndexesList(X, gatelist, show=True, exitchannel = "FITC-A"):
# Returns a bool list with gated elements on X  
    Nplotpoints = 5000 # Maximum number of points to plot per graph  
    if show == True:
        ncols = 2
        nrows = (len(gatelist)+ncols-1)/ncols # integer division
        nrows = nrows + 1 # one additional row for the GFP histogram
        gs = gridspec.GridSpec(nrows, ncols)
        gs.update(wspace=0.2, hspace=0.5)
    ax = []
    totalgatedbool = [True]*len(X) # list after all gates applied
    gatedlist = [] # list of lits with the results of every the gates applied
    print "Gating data for output in channel ", exitchannel
    print "Original number of data: ", len(X)
    print "Orignal aver/median "+exitchannel+":", np.mean(X["FITC-A"][totalgatedbool]),
    print np.median(X["FITC-A"][totalgatedbool])

    for igate, gate in enumerate(gatelist):
        gatedlist.append({"list": GatedIndexes(X, gate["polygon"],
                                               gate["channelx"], gate["channely"]),
                          "color": gate["color"]})        
        totalgatedbool = np.logical_and(totalgatedbool, gatedlist[-1]["list"])
        print "Analizing gate ", igate 
        print "   Gated cells:", sum(totalgatedbool)
        print "   Aver/Median FITC:", np.mean(X["FITC-A"][totalgatedbool]),
        print np.median(X["FITC-A"][totalgatedbool])

        if show is True:
            zorder = 0 # to keep more important points on top
            # print igate % ncols, igate/ncols
            ax.append(plt.subplot(gs[igate/ncols, igate%ncols]))
            ax[-1].ticklabel_format(style='sci', axis='both', scilimits=(0,0))
            plotgated = np.array(totalgatedbool)
            plotgated[Nplotpoints:] = [False] * len(plotgated[Nplotpoints:])
            ax[igate].plot(X[gate["channelx"]][plotgated],
                           X[gate["channely"]][plotgated],
                           'o', markersize=2, color=gate["color"],
                           alpha=0.2, zorder=zorder)
            alreadyplotted = np.array(totalgatedbool)
            print "sumtotalgatedbool", sum(totalgatedbool)
            for gated in reversed(gatedlist[:-1]):
                zorder = zorder - 1
                plotnow = np.logical_xor(gated["list"],alreadyplotted)
                plotnow[Nplotpoints:] = [False] * len(plotnow[Nplotpoints:])
                ax[igate].plot(X[gate["channelx"]][plotnow],
                               X[gate["channely"]][plotnow],
                               'o', markersize=2, color=gated["color"],
                               alpha=0.2, zorder=zorder)
                alreadyplotted =  np.logical_or(alreadyplotted, plotnow)
            nongated = np.logical_not(alreadyplotted)  # points not selected this time 
            zorder = zorder-1
            nongated[Nplotpoints:] = [False] * len(nongated[Nplotpoints:])
            ax[igate].plot(X[gate["channelx"]][nongated],
                           X[gate["channely"]][nongated],
                           'ko', markersize=2, alpha=0.2, zorder=zorder)
            polycurvex = list(zip(*gate["polygon"])[0])
            polycurvex.append(polycurvex[0])
            polycurvey = list(zip(*gate["polygon"])[1])
            polycurvey.append(polycurvey[0])
            ax[igate].plot(polycurvex,polycurvey,'-',color=gate["color"])
            ax[igate].set_xlabel(gate["channelx"])
            ax[igate].set_ylabel(gate["channely"])
    if show is True:  # Final plot of histogram
#        print "sumtotalgatedbool", sum(totalgatedbool)
        ax.append(plt.subplot(gs[-1, :]))
        histfinal = np.array([xi for xi in X[exitchannel][totalgatedbool] if xi>0.0])
        histinitial = np.array([xi for xi in X[exitchannel] if xi>0.0])
        ax[-1].hist(np.log(histfinal), 100, color=gatedlist[-1]["color"], edgecolor="none", normed=True)
        ax[-1].hist(np.log(histinitial), 100, color="k", normed=True, alpha = 0.5)
        ax[-1].set_xlabel("xcol")
        ax[-1].set_ylabel("PDF")
        plt.show()
    return totalgatedbool