import numpy as np
from scipy.optimize import curve_fit, fsolve
from scipy.interpolate import interp1d
from scipy.interpolate import UnivariateSpline
from scipy.stats import gamma, genlogistic, norm
from scipy.signal import argrelextrema
from scipy.signal import savgol_filter
import sys

logicledefaultdict = {"T": 9.0E6, "M": 6.0, "r": -100.0, "A": -100.0}


def logicle(X, pardict=logicledefaultdict, dir="forward"):
    if dir == "forward":
        return logicleforwards(X, pardict)
    else:
        return logiclebackwards(X, pardict)


def logicleforwards(X, pardict=logicledefaultdict, smooth = 0.1):
    # the forward transf requires to inver the backwards
    # this is done by interpolation
    # and attribute is created the first time the function is accessed
    Smax = 5.0
    Smin = 0.0

    if ((not hasattr(logicleforwards, "interp")) or
        (logicleforwards.pars != pardict)):
            logicleforwards.pars = dict(pardict)
            S = np.linspace(Smin, Smax, 50000)
            I = logiclebackwards(S, pardict)
            logicleforwards.interp = UnivariateSpline(I, S, s=smooth)
    SX = logicleforwards.interp(X)
#    return SX[np.logical_and((SX>Smin),(SX<Smax))]
    return SX


def logiclebackwards(X, pardict=logicledefaultdict):
    #   brings back the transformed signal to Intensity
    T = pardict["T"]
    M = pardict["M"]
    W = (M - np.log10(T/np.abs(pardict["r"])))/2.0
    A = pardict["A"]
    thr = W + A
    p = fsolve(lambda p: 2*p*np.log10(p)/(p+1)-W, 1.0)
    #print "Try solve", p, W, 2*p*np.log10(p)/(p+1)
    Yinit = np.array(X)
    Yfinal = np.array(X)
    # print T, M, W, A, thr, p
    if any(Yinit > thr):
        y = Yinit[Yinit > thr]
        Yfinal[Yinit > thr] = T*np.power(10,(thr - M))*(np.power(10,(y - thr)) -
                    p * p * np.power(10,((thr - y)/p)) + p * p - 1)
    if any(Yinit < (thr)):
        y = Yinit[Yinit < thr]
        Yfinal[Yinit < thr] = -T*np.power(10,(thr-M))*(np.power(10,(thr-y)) -
                        p * p * np.power(10,((y - thr) / p)) + p * p - 1)
    return Yfinal


def logiclebackwardsderivative(X, pardict=logicledefaultdict):
    #   change of signal per unit of change of transf
    T = pardict["T"]
    M = pardict["M"]
    W = (M - np.log10(T/np.abs(pardict["r"])))/2.0
    A = pardict["A"]
    thr = W + A
    p = fsolve(lambda p: 2*p*np.log10(p)/(p+1)-W, 1.0)
    print "Try solve", p, W, 2*p*np.log10(p)/(p+1)
    Yinit = np.array(X)
    Yfinal = np.array(X)
    # print T,M,W,A,thr,p
    if any(Yinit > thr):
        y = Yinit[Yinit > thr]
        Yfinal[Yinit > thr] = T*np.log(10)*10**(thr - M)*(10**(y - thr) +
                    p * 10**((thr - y)/p))
    if any(Yinit < (thr)):
        y = Yinit[Yinit < thr]
        Yfinal[Yinit < thr] = T*np.log(10)**(thr-M)*(10**(thr-y) +
                        p * 10**((y - thr) / p))
    return Yfinal


# backward transform of a curv C at point X to intensity units
def logiclebackwardscurvature(X, C, pardict=logicledefaultdict):
    # for each point X in transf units and curvature c
    # it returns the curvature in units of intensity
    # also returns the std dev of a normal pdf that
    # in intensity units has the computed curvature
    curvature = C*np.power(logiclebackwardsderivative(X), -2.0)
    sigma = np.power(2.0/np.pi/(curvature*curvature), 1.0/6.0)
    return curvature, sigma

def symlogtransform(X, thr=10, cut=-10, direction="forward"):
    # transform data logarythmically away from [-thr,thr]
    # it creates  ugly effects close to the 0
    # it is better the control offered by the logicle transf
#    print "lenX", len(X), type(X)
    Y = np.array(X)
    Y = Y[Y > cut]
    if direction == "forward":
        if any(Y > thr):
            Y[Y > thr] = thr*(np.log(Y[Y > thr]/thr)+1.0)
        if any(Y < (-thr)):
            Y[Y < (-thr)] = (-thr)*(np.log(-Y[Y < (-thr)]/thr)+1.0)
        return Y
    elif direction == "backward":
        print "back", X
        if any(Y > thr):
            Y[Y > thr] = thr*np.exp(Y[Y > thr]/thr-1)
        if any(Y < (-thr)):
            Y[Y < (-thr)] = -thr*np.exp(-Y[Y > (-thr)]/thr-1)
        return Y
    else:
        sys.exit("transformations can only by backward or forward")
