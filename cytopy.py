 #! /bin/sh
""":"
exec ipython -i $0 ${1+"$@"}
"""

# --matplotlib=qt for independent plot window

import FlowCytometryTools as FCT
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams["pdf.fonttype"] = 42

from mpl_toolkits.mplot3d import Axes3D

import numpy as np
import sys
import os
from scipy.optimize import curve_fit, fsolve
from scipy.interpolate import UnivariateSpline
from scipy.stats import gamma, genlogistic, norm
from scipy.signal import argrelextrema
from scipy.signal import savgol_filter
from logiclelib import *
from cytoolstats import *
from cytogates import *

import json
import seaborn as sns


#folder = "voltages"
#experiment = "20160903RPLSR/"
#path = folder + "/" + experiment
global folder
folder = "" # Unless defined, default is current
samples = []
negative = None # value of the negative control

# data = "Specimen_001_C0_001.fcs"
# datafile = folder + experiment + data

# sampledict contains all the samples to be extracted, each entry
# should coincide with a name from the FCS file
# and a set of properties that will be computed in the script
# and stored in the final json file

initsampledict = {  # Write a list with the desired experiments
 #   "A0": {"Ara": 5E-1, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
 #   "A1": {"Ara": 25E-2, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
 #   "A2": {"Ara": 5E-2, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
 #   "A3": {"Ara": 25E-3, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
 #   "A4": {"Ara": 5E-3, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
 #   "A5": {"Ara": 25E-4, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
 #   "A6": {"Ara": 5E-4, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
 #   "A7": {"Ara": 25E-5, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
 #   "A8": {"Ara": 5E-5, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
 #   "AINF": {"Ara": 0.0, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
 #   "B0": {"Ara": 5E-1, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
    # "B1": {"Ara": 25E-2, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
    # "B2": {"Ara": 5E-2, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
    # "B3": {"Ara": 25E-3, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
    # "B4": {"Ara": 5E-3, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
    # "B5": {"Ara": 25E-4, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
    # "B6": {"Ara": 5E-4, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
    # "B7": {"Ara": 25E-5, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
    # "B8": {"Ara": 5E-5, "aTc": 0.2, "AHL": 0.0, "IPTG": 100},
    # "BINF": {"Ara": 0.0, "aTc": 0.2, "AHL": 0.0, "IPTG": 100}
       }

gatelist=[]
#Add_Gate([[25000,25000],[65000,75000],[85000,50000]],"FSC-A","SSC-A",gatelist)
#Add_Gate([[30000,50000],[70000,50000],[70000,75000],[30000,75000]],"FSC-A","FSC-W",gatelist)
#Add_Gate([[40000,50000],[90000,50000],[90000,90000],[40000,90000]],"SSC-A","SSC-W",gatelist)
#datadict = {}  # This will contain the loaded FCS files 


# Function to load FCS files into datadict if they are mentioned in sampledict
# If new=True, all FCS files in folder will be loaded without sampledict
def loadfiles(foldername, rest={""}, new=False, overwrite=True):

    global folder
    folder = foldername
    global datadict
    global sampledict
    global gatelist
    sampledict = dict(initsampledict)
    datadict = {}
    if overwrite is True:
        try:  # Check for a *.in, if it exists load sampledict and gatelist from it
            print folder+'/'+folder+".in"
            execfile(folder+'/'+folder+".in",globals())
            print "sampledict and gatelist loaded from folder"
        except:
            print "not available input file"
    FCSpath = folder + "/FCS"
    for file in os.listdir(FCSpath):  # load .fcs in folder
        try:
            sample = FCT.FCMeasurement(ID="measure", datafile=FCSpath+"/"+file)
            print file,
            tubename = sample.meta["TUBE NAME"]
            if ((tubename in sampledict.keys()) or new is True):
                # new option allows to add non-considered samples
                # but they will lack info on chemicals used
                if new is True:
                    sampledict[tubename] = {}
                sampledict[tubename]["file"] = file
                datadict[tubename] = sample
                print "... Added!"
            else:
                print "... Ignored"
        except:
            pass


# Function to write sampledict into json file
def writefile(foldername=None):
    if foldername is None:
        foldername = folder
        with open(foldername+"/"+foldername+".json", "w") as fp:
        #      for samp in sampledict:
            json.dump(sampledict, fp, sort_keys=True, indent=4)

#            outputdict[]

        #           for key in sampledict[samp]:
        #               f.write(str(sampledict[samp][key])+" ")
        #           f.write("\n")


# Function to extract statsticial information from the loaded data
# extlist is a list of experiments to extract from
# transf is the transformation used to extract the data e.g. logicle
#    since data shows logarythmic variations and a linear histogram
#    would lose some of the information
# fitfunc is the basic fitting used to extract the data e.g. sumgenlog
def extractfromsample(extlist="all", channel="FITC-A", Nbins=250,
                      gatelist=gatelist, transf=logicle,
                      show=True, fitfunc=sumgenlog):
    global negative
    if extlist == "all":
        extlist = datadict.keys()
#        if "G0" in extlist:
#            extlist.remove("G0") # G0 is handled separately

    if "G0" in datadict:  # If there is a negative control in the list
        if negative is True:  # If the negative cotrol is not read before
              
        # Also the Voltage info is extracted used later for calibration
            Voltages = datadict["G0"].meta["_channels_"]["$PnV"]
            Names = datadict["G0"].meta["_channels_"]["$PnN"]
            # Use proper dataframe for next line!!!
            VFITC = float(next(Voltages[i] for i in
                               range(len(Voltages))[1:] if Names[i] == "FITC-A"))
            sampledict["G0"]["VFITC"] = VFITC
            gatedidx = GatedIndexesList(datadict["G0"].data, gatelist,
                                        show=True, exitchannel=channel)
            X = datadict["G0"].data[channel].values[gatedidx]
            accepted = str(raw_input("Accept data for baseline?: "))
            if accepted not in ["Y", 'y', ""]:
                print "Baseline extraction aborted"
                return 0
            fitting = distfit(X, transf=transf, show=show, initialV=VFITC,
                              fitfunc=fitfunc, Nbins=Nbins)
            sampledict["G0"].update(fitting)
            print "Baseline Updated!", sampledict["G0"]


    #  print fitting
    for sample in extlist:
        print sample
 
        # Also the Voltage info is extracted used later for calibration
        Voltages = datadict[sample].meta["_channels_"]["$PnV"]
        Names = datadict[sample].meta["_channels_"]["$PnN"]
        # Use proper dataframe for next line!!!
        VFITC = float(next(Voltages[i] for i in
                           range(len(Voltages))[1:] if Names[i] == "FITC-A"))
        sampledict[sample]["VFITC"] = VFITC

        gatedidx = GatedIndexesList(datadict[sample].data, gatelist,
                                    show=True, exitchannel=channel)
        accepted = str(raw_input("Accept data?: "))
        if accepted not in ["Y", 'y', ""]:
            print "Extraction aborted"
            return 0
        X = datadict[sample].data[channel].values[gatedidx]
#        X = datadict[sample].data[channel].values
        fitting = distfit(X, transf=transf, show=show, initialV=VFITC,
                          fitfunc=fitfunc, Nbins=Nbins)
        # fitting contains a set of measures that will be added to sampledict
        sampledict[sample].update(fitting)
        print "Updated!", sampledict[sample]
    return 1


# This returns a set of pars for initialazing the curve_fit of different
# target functions. Parameters here have been chosen to reproduce approximately
# the curves expected for different fittings with different transformations
def getinitpars(func, Npeaks):

    p00genlog2 = [0.645, 0.169, 1.57, 0.28,
                  0.30, 0.22, 2.8, 0.7]
    p0infgenlog2 = [0.00, 0.00, 0.00, 0.0,
                    0.00, 0.00, 0.00, 0.0]
    p0supgenlog2 = [10.0, 10.0, 10.0, 10.0,
                    10.0, 10.0, 10.0, 10.0]

    p00genlog = p00genlog2[:4]
    p0infgenlog = p0infgenlog2[:4]
    p0supgenlog = p0supgenlog2[:4]

    # p00sumnorm2 = [1.0, 1.0, 0.5, 2.0, 2.0, 0.5]
    # p0infsumnorm2 = [-1.0, 0.0, 0.0, -1.0, 0.0, 0.0]
    # p0supsumnorm2 = [5.0, 10.0, 10.0, 5.0, 10.0, 10.0]

    # p00sumnorm = p00sumnorm2[:3]
    # p0infsumnorm = p0infsumnorm2[:3]
    # p0supsumnorm = p0supsumnorm2[:3]

    p00sumnorm2 = [1.0, 1.0, 0.0, 0.5, 2.0, 2.0, 0.0, 0.5]
    p0infsumnorm2 = [-1.0, 0.0, -10.0, 0.0, -1.0, 0.0, -10.0, 0.0]
    p0supsumnorm2 = [5.0, 10.0, 10.0, 10.0, 5.0, 10.0, 10.0, 10.0]

    p00sumnorm = p00sumnorm2[:4]
    p0infsumnorm = p0infsumnorm2[:4]
    p0supsumnorm = p0supsumnorm2[:4]

    if func == sumnorm:
        if Npeaks == 1:
            return p00sumnorm, p0infsumnorm, p0supsumnorm
        elif Npeaks == 2:
            return p00sumnorm2, p0infsumnorm2, p0supsumnorm2
    elif func == sumgenlog:
        if Npeaks == 1:
            return p00genlog, p0infgenlog, p0supgenlog
        elif Npeaks == 2:
            return p00genlog2, p0infgenlog2, p0supgenlog2
    # if nothing has been returned
    print "Preexistent initial condition not found, using default"
    return None, -np.inf, np.inf


# fitting of the data to a distribution and get information from it
# such as postion of maxima and minima and their curvature
def distfit(X, npeaks=2, Nbins=250, initialV= False, finalV= 588.0,
            show=False, transf=None, fitfunc=sumnorm):
    pars = [None for i in range(npeaks)]  # models
    bic = pars[:]  # Bayesian information criterion
    # will compare the fitting function to the case with different
    # number of peaks and computing the bic (bayesian inference criterion)
    # on each of the fits

    if finalV != initialV: # Voltage correction between measures
        X = sV(X,initialV,finalV)

    if transf:
        X = transf(X)  # data is transformed before fitting
        if transf == logicle:
            X = X[X > 0]  # This to get rid of the outlayers

    hy, hx = np.histogram(X, Nbins, normed=True)
    hx = hx[:-1]+0.5*(hx[1]-hx[0])  # centre of bins
    hx = hx[1:-1]
    hy = hy[1:-1]  # Avoid weird effect in boundaries

    for imodel, npeak in enumerate(np.arange(1, npeaks+1)):
        # each imodel is a model of type fitfunc with npeak
        # number of independent components
        p0, p0inf, p0sup = getinitpars(fitfunc, npeak)
        pars[imodel], pcovar = curve_fit(fitfunc, hx, hy, p0=p0,
                                         bounds=(p0inf, p0sup))
        bic[imodel] = bicf(hx, hy, fitfunc, pars[imodel])
        print "model with ", npeak, " components fitted! bic: ", bic[imodel]
    bestpars = pars[np.argmin(bic)]  # Chosen model witn minimum bic
    print "Best model parameters: ", bestpars
#    stats = getstats(fitfunc, bestpars)   
    # Looking for maxima and minima of fitted function
    xfit = np.linspace(np.min(X), np.max(X), 50000)
    yfit = fitfunc(xfit, *bestpars) # reproduction of the best fit
    imax = argrelextrema(yfit, np.greater)[0]
    imin = argrelextrema(yfit, np.less)[0]
    stats = {"xfitmax": xfit[imax], "xfitmin": xfit[imin],
             "yfitmax": yfit[imax], "yfitmin": yfit[imin]}
    print "xfitpos:", stats
    # tolist() enables json output later
    # Refining maxima and finding curvature
    # It fits sequentially second order polynomia around the found xfitmax
    xwindow = 0.01  # window to look to refine minimax
    errwindow = 0.01  # Threshold distance to accept minimax
    if len(imax) > 0:
        fitcurv = [None for x in imax]
        for iximax, ximax in enumerate(stats["xfitmax"]):
            fitcurv[iximax] = refineminimax(xfit, yfit, ximax, errwindow, xwindow)
        stats["max"] = fitcurv
    if len(imin) > 0:
        fitcurv = [None for x in imin]
        for iximin, ximin in enumerate(stats["xfitmin"]):
            fitcurv[iximin] = refineminimax(xfit, yfit, ximin, errwindow, xwindow)
        stats["min"] = fitcurv
    # Computing probabilities to be either side of minimum
    # This may be important since intensity transformation can be defective
    if "xfitmin" in stats.keys():
        listmin=[0]+list(stats["xfitmin"])+[np.max(X)]
        probs = [0 for x in range(len(stats["xfitmin"]) + 1)]
        for p in range(len(probs)):
            probs[p] = sum(1 for xi in X if (xi > listmin[p] and xi<listmin[p+1]))
            probs[p] = (1.0*probs[p]) / len(X)
    else:
        probs = [1.0]
    for iel,el in enumerate(stats["max"]):
        stats["max"][iel]["prob"]=probs[iel]
    # Plotting
    if show is True:
        curvelength = 0.3/2.0 # xlength to plot curvatures
        # Plot of FCS data
        plt.hist(X, 100, normed=True, color="b",edgecolor="b", alpha=0.3)
        plt.plot(hx,hy,'bo',markersize=2)
        # Plot of fitted function
        plt.plot(xfit, yfit, 'r-', lw=3)
        # Plot of fitted function components
        for igamma in range(len(bestpars)/4):
            ycomponent = fitfunc(xfit, *bestpars[4*igamma:4*(igamma+1)])
            plt.plot(xfit, ycomponent, "r--", lw=2)
        for imax, max in enumerate(stats["xfitmax"]):
            plt.plot(stats["max"][imax]["xpos"],
                     stats["max"][imax]["ypos"], 'go')
            xcurve = np.linspace(max-curvelength, max+curvelength, 50)
            p = stats["max"][imax]["p"]
            ycurve = p[0]*xcurve*xcurve + p[1]*xcurve + p[2]
            plt.plot(xcurve, ycurve, "g", lw=2)
        for imin, min in enumerate(stats["xfitmin"]):
            plt.plot(stats["min"][imin]["xpos"],
                     stats["min"][imin]["ypos"], 'go')
            xcurve = np.linspace(min-curvelength, min+curvelength, 50)
            p = stats["min"][imin]["p"]
            ycurve = p[0]*xcurve*xcurve + p[1]*xcurve + p[2]
            plt.plot(xcurve, ycurve, "g", lw=2)
#        plt.savefig("Ara0fit.pdf")
        plt.show()
    # creation of final dictionary before returning, not all stats will
    # be used neither are in fluorescence units
    outstats = {}
    checklist = ["min", "max"]
    for check in checklist:
        if check in stats:
            outstats[check] = stats[check]
            for iel, el in enumerate(outstats[check]):
                tx = transf([stats[check][iel]["xpos"]], dir="backwards")
                ty = transf([stats[check][iel]["ypos"]], dir="backwards")
                del stats[check][iel]["p"]
                if transf == logicle:
                    c,s = logiclebackwardscurvature([outstats[check][iel]["xpos"]],
                                                  [outstats[check][iel]["curv"]])
                else:
                    print "Curvature transormation unknown!!!"
                    c,s = [np.nan], [np.nan]
                outstats[check][iel]["xpos"] = tx[0]
                outstats[check][iel]["ypos"] = ty[0]
                outstats[check][iel]["curv"] = c[0]
                outstats[check][iel]["std"] = s[0]
    print outstats
    return outstats


def plothisto(rest=[], gate=True):
    fig = plt.figure()
#    ax = fig.add_subplot(111,projection='3d')
    for el in datadict:
       if el in rest:
        if gate:
            gatedidx = GatedIndexesList(datadict[el].data, gatelist,
                                    show=True, exitchannel="FITC-A")
            X = datadict[el].data["FITC-A"].values[gatedidx]
        else:
            X = datadict[el].data["FITC-A"].values
        print len(X)
        X = X[X>0]
        X = np.log10(X)
        plt.hist(X,200,normed=True, color = '#8ce500')
    plt.gca().set_xlim(1.5, 5)
    plt.gca().set_xlabel('Intensity')
    plt.gca().set_ylabel('$Probability$')
#    ax.set_ylim3d(-7, 0)
#    plt.savefig("histo3d.pdf") 
    plt.savefig("histo.pdf")



def plot3dhisto(order="Ara", rest={}, transf=None):
    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')
    cmap = mpl.cm.get_cmap('Set1')


    MaxAra = np.log10(max([sampledict[el]["Ara"] for el in datadict
                    if sampledict[el]["Ara"] > 0.0]))
    MinAra = np.log10(min([sampledict[el]["Ara"] for el in datadict
                    if sampledict[el]["Ara"] > 0.0]))
    MinAra = MinAra - 1  # to make place to -inf
    print "MM", MaxAra,MinAra

    print len(datadict)
    histos = []
    colors = []
    zs = []
    for el in datadict:
        if el[0]=="A":

            X = datadict[el].data["FITC-A"].values
            if transf:
                X = transf(X)
            Ara = np.log10(sampledict[el]["Ara"])
            if Ara < MinAra: # probably the Ara0
                Ara=MinAra-1
            print el, Ara
            color = cmap((2.0*(MaxAra-Ara)/(MaxAra-MinAra))%1)
            hist, bins = np.histogram(X, bins=200, normed=True)
            xs = (bins[:-1] + bins[1:])/2
            hist[0] = 0
            hist[-1] = 0
            histos.append(list(zip(xs,hist)))
            colors.append(color)
            zs.append(Ara)
    print len(histos)       
    poly = mpl.collections.PolyCollection(histos, facecolors=colors,
                                          edgecolors=colors)
    poly.set_alpha(0.6)
    ax.add_collection3d(poly, zs=zs, zdir='y')

    ax.view_init(elev=40, azim=-70)
    ax.set_xlabel('$FIT-C$')
    ax.set_xlim3d(0, 700)
    ax.set_ylabel('$\log [Ara]$')
    ax.set_ylim3d(-7, 0)
    ax.set_zlabel('$Probability$')
    ax.set_zlim3d(0, 0.009)
    plt.savefig("histo3d.pdf") 
#    plt.show()

def plotV():
    X1 = []
    X2 = []
    Xm = []
    Vl = []
    for el in sampledict:
        print el
        V = sampledict[el]["VFITC"]
        X1.append(max(sampledict[el]["fit"]["max"][0]["curv"],
                      sampledict[el]["fit"]["max"][1]["curv"]))
        X2.append(min(sampledict[el]["fit"]["max"][0]["curv"],
                      sampledict[el]["fit"]["max"][1]["curv"]))
        Xm.append(sampledict[el]["fit"]["min"][0]["curv"])
        Vl.append(V)   
        plt.plot(Vl,np.log10(np.abs(X1)),'ro')
        plt.plot(Vl,np.log10(np.abs(X2)),'bo')
#        plt.plot(Vl,Xm,'bo')
    X=np.linspace(390, 1000, 10)
    Vl=np.array(Vl)
    X1=np.array(X1)
    X2=np.array(X2)
    Xm=np.array(Xm)

#    p1 = np.polyfit(Vl, np.log10(X1), 2)
#    p2 = np.polyfit(Vl[X2>100], np.log10(X2[X2>100]), 2)
#    pm = np.polyfit(Vl, np.log10(Xm), 2)    
    # plt.plot(X, p1[0]*X*X+p1[1]*X+p1[2], "-")
    # plt.plot(X, p2[0]*X*X+p2[1]*X+p2[2], "-")
    # plt.plot(X, p1[0]*X*X+p1[1]*X+p1[2]-1.15, "-")
    # plt.plot(X, p1[0]*X*X+p1[1]*X+p1[2]-0.55, "-")
    # plt.plot(X, pm[0]*X*X+pm[1]*X+pm[2], "-")
#    print p1
    plt.show()

def sV(S0,V0,V):
# From a calibrating fit of signal for different voltages
# the relationship S(V) is seen to follow a parabolla that follows
# log(S(V))= f(V) + C, with C a constant that depends on the concentration
# fluorophor. Thus, for a determined known pair S(V0)=f(V0), the signal
# at a different voltage can be obtained as S(V)= S(V0)*10**(f(V)-f(V0)) 
    print "Transforming Voltages from V0: ", V0, "to V: ", V
    fV = -3.96095274e-06*V*V + 1.04836777e-02*V
    fV0 = -3.96095274e-06*V0*V0 + 1.04836777e-02*V0
    S = S0 * np.power(10,fV-fV0)
    return S