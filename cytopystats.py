import numpy as np
from scipy.stats import gamma, genlogistic, norm


# distribution (not normalized) of normalized generic logistic functions
def sumgenlog(X, *p):
    a = p[0::4]
    s = p[1::4]
    b = p[2::4]
    w = np.array(p[3::4])
    N = len(p)/4  # Number of genlogs to fit
    return sum([w[i]*genlogistic(a[i], loc=b[i],
                                 scale=s[i]).pdf(X) for i in range(N)])


# distribution (not notmalized) of assymmetryc normal functions
# not really good since can give sporadic false minima and maxima in the tails
def sumnorm(X, *p):
    b = p[0::4]
    s = p[1::4]
    a = np.array(p[2::4])
    w = np.array(p[3::4])
    N = len(p)/4  # Number of gammas to fit
#    print b,s,w,N
#    return sum([w[i]*(a[i]*(X-b[i])+1.0)**2.0*norm(loc=b[i], scale=s[i]).pdf(X) for i in range(N)])
    return sum([w[i]*norm(loc=b[i], scale=s[i]).pdf(X) for i in range(N)])


def refineminimax(hx, hy, xinit, enddistance, window):
            # Fits a parabola over a range and finds its min/max
            # iterates keeping the center ad the new window average
            # ends when the new and old min/max are close enough
            maxiter = 20
            iter = 0
            xmm = xinit
            currentdistance = 2.0 * enddistance
            print "Entering loop"
            while((abs(currentdistance) > enddistance) and (iter < maxiter)):
                print "xmm,", xmm
                slice = np.logical_and(hx > (xmm-window), hx < (xmm+window))
                p = np.polyfit(hx[slice], hy[slice], 2)
                newxmm = -p[1]/(2.0*p[0])
                currentdistance = newxmm - xmm
                if currentdistance > (2.0 * enddistance):
                    newxmm = xmm + 2.0 * enddistance
                    currentdistance = 2.0 * enddistance
                elif currentdistance < (-2.0 * enddistance):
                    newxmm = xmm - 2.0 * enddistance
                    currentdistance = -2.0 * enddistance
                xmm = newxmm
                iter = iter + 1
            return {"p": p,
                    "xpos": -p[1]/(2.0*p[0]), 
                    "ypos": -p[1]*p[1]/(4.0*p[0])+p[2],
                    "curv": 2.0*p[0]}


def bicf(X, Y, func, p):
    yexpected = func(X, *p)
    error = np.sum((yexpected-Y)*(yexpected-Y))/len(X)
    df = len(p) - 1  # numer of parameters minus normalisation
    bic = len(X)*np.log(error) + df * np.log(len(X))
    return bic


def getstats(fitfunc, p):
    if fitfunc == sumgenlog:
        return sumlogstats(p)
    else:
        print "Fitting procedure not implemented in genstats"
        return 0

def sumlogstats(p):
    a = p[0::4]
    s = p[1::4]
    b = p[2::4]
    w = np.array(p[3::4])
    N = len(p)/4  # Number of gammas to fit
    pdfs = [genlogistic(a[i], loc=b[i], scale=s[i]) for i in range(N)]
    return {"mean":[pdf.mean() for pdf in pdfs],
            "std":[pdf.std() for pdf in pdfs],
            "median":[pdf.median() for pdf in pdfs],
            "weight":[wi for wi in w]}
